﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AdessoRideShare.Entities.Entities
{
    public class Trip : BaseEntity
    {
        [MaxLength(50)]
        public string From { get; set; } // Starting City
        [MaxLength(50)]
        public string Where { get; set; } // Destination City

        public DateTime TravelDate { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }

        public int TotalSeatNumber { get; set; }

        public int RemainingSeatNumber { get; set; }

        public string Route { get; set; }




    }
}
