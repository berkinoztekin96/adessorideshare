﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Entities.Entities
{
    public class City : BaseEntity
    {
        public string Name { get; set; }

        public string NeighborCities { get; set; }
    }
}
