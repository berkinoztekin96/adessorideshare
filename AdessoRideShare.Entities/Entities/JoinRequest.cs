﻿using AdessoRideShare.Common.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AdessoRideShare.Entities.Entities
{
    public class JoinRequest : BaseEntity
    {
        public RequestStatusEnum RequestStatus { get; set; }

        [ForeignKey("Trip")]
        public int TripId { get; set; }

        public virtual Trip Trip { get; set; }
    }
}
