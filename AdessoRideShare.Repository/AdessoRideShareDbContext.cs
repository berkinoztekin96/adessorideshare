﻿using AdessoRideShare.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Repository
{
    public class AdessoRideShareDbContext : DbContext
    {
        public AdessoRideShareDbContext(DbContextOptions<AdessoRideShareDbContext> options)
            : base(options)
        {
        }

        public DbSet<Trip> Trips { get; set; }
        public DbSet<JoinRequest> JoinRequests { get; set; }
        public DbSet<City> Cities { get; set; }

    }
}
