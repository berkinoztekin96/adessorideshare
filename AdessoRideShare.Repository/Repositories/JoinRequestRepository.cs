﻿using AdessoRideShare.Entities.Entities;
using AdessoRideShare.Repository.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Repository.Repositories
{
    public class JoinRequestRepository : Repository<JoinRequest>, IJoinRequestRepository
    {
        public JoinRequestRepository(AdessoRideShareDbContext context)
            : base(context)
        {
        }


    }
}
