﻿using AdessoRideShare.Entities.Entities;
using AdessoRideShare.Repository.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Repository.Repositories
{
    public class TripRepository : Repository<Trip>, ITripRepository
    {
        public TripRepository(AdessoRideShareDbContext context)
            : base(context)
        {
        }


    }
}
