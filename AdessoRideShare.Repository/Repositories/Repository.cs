﻿using AdessoRideShare.Entities.Entities;
using AdessoRideShare.Repository.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdessoRideShare.Repository.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly AdessoRideShareDbContext dbContext;
        internal DbSet<TEntity> dbSet;

        public Repository(AdessoRideShareDbContext AdessoRideShareDbContext)
        {
            this.dbContext = AdessoRideShareDbContext;
            this.dbSet = dbContext.Set<TEntity>();
        }


        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            try
            {
                entity.CreatedDate = DateTime.Now;
                entity.IsActive = true;
                await dbContext.Set<TEntity>().AddAsync(entity);
                await dbContext.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            try
            {
                dbContext.Set<TEntity>().Update(entity);
                await dbContext.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = GetAllAsync().Where(predicate);
            return includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public IQueryable<TEntity> GetAllAsync()
        {
            try
            {
                return dbContext.Set<TEntity>();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
