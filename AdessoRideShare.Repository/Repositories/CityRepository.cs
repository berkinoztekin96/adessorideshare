﻿using AdessoRideShare.Common.Helper;
using AdessoRideShare.Entities.Entities;
using AdessoRideShare.Repository.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AdessoRideShare.Repository.Repositories
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(AdessoRideShareDbContext context)
            : base(context)
        {
        }


        public async Task<City> GetCityByName(string cityName)
        {

            return await dbSet.FirstOrDefaultAsync(x => x.Name == cityName);
   
        }


    }

}