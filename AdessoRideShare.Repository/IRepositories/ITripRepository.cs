﻿using AdessoRideShare.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Repository.IRepositories
{
    public interface ITripRepository : IRepository<Trip>
    {
    }
}
