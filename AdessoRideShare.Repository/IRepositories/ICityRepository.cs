﻿using AdessoRideShare.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdessoRideShare.Repository.IRepositories
{
    public interface ICityRepository : IRepository<City>
    {
         Task<City> GetCityByName(string cityName);
    }
}
