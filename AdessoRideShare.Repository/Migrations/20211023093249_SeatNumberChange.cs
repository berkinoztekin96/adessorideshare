﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdessoRideShare.Repository.Migrations
{
    public partial class SeatNumberChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfSeats",
                table: "Trips");

            migrationBuilder.AddColumn<int>(
                name: "RemainingSeatNumber",
                table: "Trips",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalSeatNumber",
                table: "Trips",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RemainingSeatNumber",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "TotalSeatNumber",
                table: "Trips");

            migrationBuilder.AddColumn<int>(
                name: "NumberOfSeats",
                table: "Trips",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
