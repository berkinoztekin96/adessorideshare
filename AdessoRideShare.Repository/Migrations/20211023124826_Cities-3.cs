﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdessoRideShare.Repository.Migrations
{
    public partial class Cities3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Route",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "Coordinates",
                table: "Cities");

            migrationBuilder.AddColumn<string>(
                name: "NeighborCities",
                table: "Cities",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NeighborCities",
                table: "Cities");

            migrationBuilder.AddColumn<string>(
                name: "Route",
                table: "Trips",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Coordinates",
                table: "Cities",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
