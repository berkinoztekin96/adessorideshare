﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdessoRideShare.Repository.Migrations
{
    public partial class JoinRequestNameFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_joinRequests_Trips_TripId",
                table: "joinRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_joinRequests",
                table: "joinRequests");

            migrationBuilder.RenameTable(
                name: "joinRequests",
                newName: "JoinRequests");

            migrationBuilder.RenameIndex(
                name: "IX_joinRequests_TripId",
                table: "JoinRequests",
                newName: "IX_JoinRequests_TripId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JoinRequests",
                table: "JoinRequests",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JoinRequests_Trips_TripId",
                table: "JoinRequests",
                column: "TripId",
                principalTable: "Trips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinRequests_Trips_TripId",
                table: "JoinRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JoinRequests",
                table: "JoinRequests");

            migrationBuilder.RenameTable(
                name: "JoinRequests",
                newName: "joinRequests");

            migrationBuilder.RenameIndex(
                name: "IX_JoinRequests_TripId",
                table: "joinRequests",
                newName: "IX_joinRequests_TripId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_joinRequests",
                table: "joinRequests",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_joinRequests_Trips_TripId",
                table: "joinRequests",
                column: "TripId",
                principalTable: "Trips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
