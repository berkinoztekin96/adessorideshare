﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AdessoRideShare.Repository.Migrations
{
    public partial class Cities4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Route",
                table: "Trips",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Route",
                table: "Trips");
        }
    }
}
