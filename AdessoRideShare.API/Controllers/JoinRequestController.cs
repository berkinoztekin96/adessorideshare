﻿using AdessoRideShare.Business.IServices;
using AdessoRideShare.Common.Dto;
using AdessoRideShare.Common.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdessoRideShare.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JoinRequestController : ControllerBase
    {


        private readonly ITripService _tripService;
        private readonly IJoinRequestService _joinRequestService;
        public JoinRequestController(ITripService tripService, IJoinRequestService joinRequestService)
        {
            _tripService = tripService;
            _joinRequestService = joinRequestService;
        }




        [HttpGet("GetJoinRequests/{tripId}")]
        public async Task<Response<JoinRequestDto>> GetJoinRequests(int tripId)
        {
           
            if(tripId <= 0)
                return new Response<JoinRequestDto>() { isSuccess = false, Data = null, List = null, Message = "Id is not valid", Status = 200 };

            var serviceResult = await _joinRequestService.GetAllJoinRequests(tripId);
            if(serviceResult.isSuccess)
            {
                return new Response<JoinRequestDto>() { isSuccess = true, Data = serviceResult.Data, List = serviceResult.List, Message = serviceResult.Message, Status =serviceResult.Status };
            }
            else
                return new Response<JoinRequestDto>() { isSuccess = false, Data = serviceResult.Data, List = serviceResult.List, Message = serviceResult.Message, Status = serviceResult.Status };
        }


        [HttpGet("ConsiderJoinRequest/{requestId}")]
        public async Task<Response<JoinRequestDto>> ConsiderJoinRequest(int requestId,int statusId)
        {

            if (requestId <= 0)
                return new Response<JoinRequestDto>() { isSuccess = false, Data = null, List = null, Message = "Id is not valid", Status = 200 };

            var serviceResult = await _joinRequestService.ConsiderRequest(requestId, statusId);
            if (serviceResult.isSuccess)
            {
                return new Response<JoinRequestDto>() { isSuccess = true, Data = serviceResult.Data, List = serviceResult.List, Message = serviceResult.Message, Status = serviceResult.Status };
            }
            else
                return new Response<JoinRequestDto>() { isSuccess = false, Data = serviceResult.Data, List = serviceResult.List, Message = serviceResult.Message, Status = serviceResult.Status };
        }


        [HttpPost("CreateJoinRequest")]
        public async Task<Response<JoinRequestDto>> CreateJoinRequest([FromBody] int tripId)
        {
            if(tripId <= 0)
                return new Response<JoinRequestDto>() { isSuccess = false, Data = null, List = null, Message = "Id is not valid", Status = 200 };


            var serviceResult = await _joinRequestService.CreateJoinRequest(tripId);
            if(serviceResult.isSuccess)
            {
                return new Response<JoinRequestDto>() { isSuccess = true, Data = serviceResult.Data, List = null, Message = serviceResult.Message, Status = serviceResult.Status };
            }
            else
                return new Response<JoinRequestDto>() { isSuccess = false, Data = serviceResult.Data, List = serviceResult.List, Message = serviceResult.Message, Status = serviceResult.Status };

        }


    }
}
