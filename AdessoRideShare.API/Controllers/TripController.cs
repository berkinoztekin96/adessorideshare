﻿using AdessoRideShare.Business.IServices;
using AdessoRideShare.Common.Dto;
using AdessoRideShare.Common.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdessoRideShare.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripController : ControllerBase
    {
        private readonly ITripService _tripService;
        public TripController(ITripService tripService)
        {
            _tripService = tripService;
        }


        [HttpPost("CreateTrip")]
        public async Task<Response<TripDto>> CreateTrip([FromBody] CreateTripDto dto)
        {

            if (String.IsNullOrEmpty(dto.FromCity) || String.IsNullOrEmpty(dto.WhereCity))
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = "From or where city cannot be null", Status = 200 };

            if (dto.NumberOfSeats <= 0)
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = "Please enter valid seat number", Status = 200 };


            var serviceResult = await _tripService.CreateTrip(dto);
            if (serviceResult.isSuccess)
            {
                return new Response<TripDto>() { isSuccess = true, Data = serviceResult.Data, List = null, Message = serviceResult.Message, Status = serviceResult.Status };

            }


            else
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = serviceResult.Message, Status = serviceResult.Status };
        }



        [HttpDelete("DeleteTrip/{id}")]
        public async Task<Response<TripDto>> DeleteTrip(int id)
        {
            if (id <= 0)
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = "Id is not valid", Status = 200 };


            var serviceResult = await _tripService.DeleteTrip(id);
            if (serviceResult.isSuccess)
                return new Response<TripDto>() { isSuccess = true, Data = serviceResult.Data, List = null, Message = serviceResult.Message, Status = serviceResult.Status};

            else
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = serviceResult.Message, Status = serviceResult.Status };

        }



        [HttpPost("SearchTrip")]
        public async Task<Response<TripDto>> SearchTrip(TripSearchDto dto)
        {
            if(String.IsNullOrEmpty(dto.FromCity) || String.IsNullOrEmpty(dto.WhereCity))
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = "From or where city cannot be null", Status = 200 };

            var serviceResult = await _tripService.SearchTrip(dto);
            if(serviceResult.isSuccess)
                return new Response<TripDto>() { isSuccess = true, Data = null, List = serviceResult.List, Message = serviceResult.Message, Status = serviceResult.Status };


            else
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = serviceResult.Message, Status = serviceResult.Status };

        }

    }
}