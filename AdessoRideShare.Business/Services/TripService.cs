﻿using AdessoRideShare.Business.IServices;
using AdessoRideShare.Common.Dto;
using AdessoRideShare.Common.Helper;
using AdessoRideShare.Entities.Entities;
using AdessoRideShare.Repository;
using AdessoRideShare.Repository.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdessoRideShare.Business.Services
{
    public class TripService : ITripService
    {
        private readonly ITripRepository _tripRepository;
        private readonly ICityRepository _cityRepository;
        private readonly AdessoRideShareDbContext _dbContext;
        public TripService(ITripRepository tripRepository, AdessoRideShareDbContext dbContext, ICityRepository cityRepository)
        {
            _tripRepository = tripRepository;
            _dbContext = dbContext;
            _cityRepository = cityRepository;
        }

        public async Task<Response<TripDto>> CreateTrip(CreateTripDto dto)
        {
            try
            {

                Trip trip = new Trip()
                {
                   
                    Description = dto.Description,
                    From = dto.FromCity,
                    Where = dto.WhereCity,
                    RemainingSeatNumber = dto.NumberOfSeats,
                    TotalSeatNumber = dto.NumberOfSeats,
                    TravelDate = dto.TripDate,
                    Route = dto.Route
                };
                await _tripRepository.CreateAsync(trip);

                TripDto returnDto = new TripDto()
                {
                    Description = dto.Description,
                    FromCity = dto.FromCity,
                    RemainingSeats = dto.NumberOfSeats,
                    TripDate = dto.TripDate,
                    WhereCity = dto.WhereCity,
                    Id = trip.Id,
                    Route = dto.Route

                };

                return new Response<TripDto>() { isSuccess = true, Data = returnDto, List = null, Message = "Success", Status = 200 };

            }
            catch (Exception ex)
            {

                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }
        }

        public async Task<Response<TripDto>> DeleteTrip(int id)
        {

            try
            {
                var trip = await _tripRepository.FindBy(x => x.Id == id).FirstOrDefaultAsync();
                if (trip == null)
                    return new Response<TripDto>() { isSuccess = true, Data = null, List = null, Message = "Trip does not found", Status = 200 };


                trip.IsActive = false;

                await _tripRepository.UpdateAsync(trip);


                TripDto returnDto = new TripDto()
                {
                    Description = trip.Description,
                    FromCity = trip.From,
                    TotalSeatNumber = trip.TotalSeatNumber,
                    RemainingSeats = trip.RemainingSeatNumber,
                    TripDate = trip.TravelDate,
                    WhereCity = trip.Where,
                    Id = trip.Id,
                    Route = trip.Route
                };

                return new Response<TripDto>() { isSuccess = true, Data = returnDto, List = null, Message = "Success", Status = 200 };

            }
            catch (Exception ex)
            {
                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };

            }


        }

        public async Task<Response<TripDto>> GetAllTrips()
        {
            try
            {

                List<TripDto> listDto = new List<TripDto>();
                var tripList = await _tripRepository.FindBy(x => x.IsActive == true).ToListAsync();

                if (tripList.Count <= 0)
                    return new Response<TripDto>() { isSuccess = true, Data = null, List = listDto, Message = "No trip found", Status = 200 };

                foreach (var item in tripList)
                {
                    if (item.IsActive == true)
                    {

                        TripDto dto = new TripDto()
                        {
                            Description = item.Description,
                            FromCity = item.From,
                            Id = item.Id,
                            RemainingSeats = item.RemainingSeatNumber,
                            TotalSeatNumber = item.TotalSeatNumber,
                            TripDate = item.TravelDate,
                            WhereCity = item.Where
                        };

                        listDto.Add(dto);
                    }
                }


                return new Response<TripDto>() { isSuccess = true, Data = null, List = listDto, Message = "Success", Status = 200 };

            }
            catch (Exception ex)
            {

                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }

        }



        public async Task<Response<TripDto>> GetTripById(int id)
        {
            try
            {

                Trip trip = await _tripRepository.FindBy(x => x.Id == id).FirstOrDefaultAsync();
                if (trip == null)
                    return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = "Trip does not found", Status = 200 };


                TripDto returnDto = new TripDto()
                {
                    Description = trip.Description,
                    FromCity = trip.From,
                    WhereCity = trip.Where,
                    Id = trip.Id,
                    RemainingSeats = trip.RemainingSeatNumber,
                    TotalSeatNumber = trip.TotalSeatNumber,
                    TripDate = trip.TravelDate
                };

                return new Response<TripDto>() { isSuccess = false, Data = returnDto, List = null, Message = "Success", Status = 200 };


            }
            catch (Exception ex)
            {

                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }

        }

        public async Task<Response<TripDto>> UpdateTrip(UpdateTripDto dto)
        {
            try
            {
                Trip trip = await _tripRepository.FindBy(x => x.Id == dto.Id && x.IsActive == true).FirstOrDefaultAsync();
                if (trip == null)
                    return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = "Trip does not found", Status = 200 };

                //update part
                trip.Description = dto.Description;
                trip.From = dto.FromCity;
                trip.Where = dto.WhereCity;
                trip.TotalSeatNumber = dto.TotalSeatNumber;
                trip.RemainingSeatNumber = dto.RemainingSeats;
                trip.TravelDate = dto.TripDate;

                await _tripRepository.UpdateAsync(trip);

                TripDto returnDto = new TripDto()
                {
                    Description = trip.Description,
                    FromCity = trip.From,
                    Id = trip.Id,
                    RemainingSeats = trip.RemainingSeatNumber,
                    TotalSeatNumber = trip.TotalSeatNumber,
                    TripDate = trip.TravelDate,
                    WhereCity = trip.Where
                };
                return new Response<TripDto>() { isSuccess = false, Data = returnDto, List = null, Message = "Success", Status = 200 };


            }
            catch (Exception ex)
            {

                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }
        }

        public async Task<Response<TripDto>> SearchTrip(TripSearchDto dto)
        {
            try
            {
                List<TripDto> listDto = new List<TripDto>();
                var startDate = new DateTime(dto.TripDate.Year, dto.TripDate.Month, dto.TripDate.Day, 00, 00, 00);
                var endDate = new DateTime(dto.TripDate.Year, dto.TripDate.Month, dto.TripDate.Day, 23, 59, 59);

                var tripList = await _tripRepository.FindBy(x => x.IsActive == true && x.TravelDate >= startDate && x.TravelDate <= endDate).ToListAsync();


                foreach (var item in tripList)
                {
                    var routeCity = item.Route.Split('-').ToList();

                    foreach (var cityName in routeCity)
                    {
                        var city = await _cityRepository.GetCityByName(cityName);
                        if (city == null)
                            break;

                        if(city.NeighborCities.Contains(dto.FromCity) && routeCity.Last() == dto.WhereCity)
                        {
                            TripDto tripDto = new TripDto()
                            {
                                Description = item.Description,
                                FromCity = item.From,
                                Id = item.Id,
                                RemainingSeats = item.RemainingSeatNumber,
                                TotalSeatNumber = item.TotalSeatNumber,
                                TripDate = item.TravelDate,
                                WhereCity = item.Where,
                                Route = item.Route
                            };
                            listDto.Add(tripDto);
                            break;


                        }
                    }
                 
                }          
                //foreach (var item in tripList)
                //{
                   
                //        TripDto tripDto = new TripDto()
                //        {
                //            Description = item.Description,
                //            FromCity = item.From,
                //            Id = item.Id,
                //            RemainingSeats = item.RemainingSeatNumber,
                //            TotalSeatNumber = item.TotalSeatNumber,
                //            TripDate = item.TravelDate,
                //            WhereCity = item.Where
                //        };
                //        listDto.Add(tripDto);
                   
                //}
                return new Response<TripDto>() { isSuccess = true, Data = null, List = listDto, Message = "Success", Status = 200 };


            }
            catch (Exception ex)
            {

                return new Response<TripDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }



        }

    }
}
