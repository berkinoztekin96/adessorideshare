﻿using AdessoRideShare.Business.IServices;
using AdessoRideShare.Common.Dto;
using AdessoRideShare.Common.Helper;
using AdessoRideShare.Entities.Entities;
using AdessoRideShare.Repository.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdessoRideShare.Business.Services
{
    public class JoinRequestService : IJoinRequestService
    {
        private readonly ITripRepository _tripRepository;
        private readonly IJoinRequestRepository _joinRequestRepository;

        public JoinRequestService(ITripRepository tripRepository, IJoinRequestRepository joinRequestRepository)
        {
            _tripRepository = tripRepository;
            _joinRequestRepository = joinRequestRepository;
        }



        public async Task<Response<JoinRequestDto>> CreateJoinRequest(int tripId)
        {
            try
            {

                Trip trip = await _tripRepository.FindBy(x => x.Id == tripId).FirstOrDefaultAsync();

                if (trip == null)
                    return new Response<JoinRequestDto>() { isSuccess = true, Data = null, List = null, Message = "Trip does not found", Status = 200 };


                if (trip.RemainingSeatNumber <= 0)
                    return new Response<JoinRequestDto>() { isSuccess = true, Data = null, List = null, Message = "No seat avaiable", Status = 200 };

                JoinRequest joinRequest = new JoinRequest()
                {
                    IsActive = true,
                    RequestStatus = Common.Enum.RequestStatusEnum.Waiting,
                    TripId = tripId
                };


                await _joinRequestRepository.CreateAsync(joinRequest);


                JoinRequestDto returnDto = new JoinRequestDto()
                {
                    CreatedDate = joinRequest.CreatedDate,
                    Id = joinRequest.Id,
                    RequestStatus = joinRequest.RequestStatus,
                    TripId = tripId
                };

                return new Response<JoinRequestDto>() { isSuccess = true, Data = returnDto, List = null, Message = "Success", Status = 200 };

            }
            catch (Exception ex)
            {

                return new Response<JoinRequestDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }

        }

        public async Task<Response<JoinRequestDto>> GetAllJoinRequests(int tripId)
        {

            try
            {
                List<JoinRequestDto> listDto = new List<JoinRequestDto>();
                Trip trip = await _tripRepository.FindBy(x => x.Id == tripId).FirstOrDefaultAsync();

                if (trip == null)
                    return new Response<JoinRequestDto>() { isSuccess = true, Data = null, List = null, Message = "Trip does not found", Status = 200 };


                var joinRequestList = await _joinRequestRepository.FindBy(x => x.RequestStatus == Common.Enum.RequestStatusEnum.Waiting).ToListAsync();

                foreach (var item in joinRequestList)
                {
                    JoinRequestDto dto = new JoinRequestDto()
                    {
                        CreatedDate = item.CreatedDate,
                        Id = item.Id,
                        RequestStatus = item.RequestStatus,
                        TripId = item.TripId
                    };

                    listDto.Add(dto);
                }

                return new Response<JoinRequestDto>() { isSuccess = true, Data = null, List = listDto, Message = "Success", Status = 200 };


            }
            catch (Exception ex)
            {

                return new Response<JoinRequestDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }


        }


        public async Task<Response<JoinRequestDto>> ConsiderRequest(int requestId, int status)
        {
            try
            {
                JoinRequest joinRequest = await _joinRequestRepository.FindBy(x => x.Id == requestId).FirstOrDefaultAsync();

                if (joinRequest == null)
                    return new Response<JoinRequestDto>() { isSuccess = true, Data = null, List = null, Message = "Request does not found", Status = 200 };

                Trip trip = await _tripRepository.FindBy(x => x.Id == joinRequest.TripId).FirstOrDefaultAsync();
                if (trip == null)
                    return new Response<JoinRequestDto>() { isSuccess = true, Data = null, List = null, Message = "Trip does not found", Status = 200 };

                if (status == 1)
                {
                    if(trip.RemainingSeatNumber <= 0)               
                        return new Response<JoinRequestDto>() { isSuccess = true, Data = null, List = null, Message = "No seat avaiable", Status = 200 };
                    

                    joinRequest.RequestStatus = Common.Enum.RequestStatusEnum.Accepted;
                    trip.RemainingSeatNumber--;
                }

                else
                    joinRequest.RequestStatus = Common.Enum.RequestStatusEnum.Rejected;

                await _joinRequestRepository.UpdateAsync(joinRequest);

               

            

                await _tripRepository.UpdateAsync(trip);


                JoinRequestDto returnDto = new JoinRequestDto()
                {
                    CreatedDate = joinRequest.CreatedDate,
                    Id = joinRequest.Id,
                    RequestStatus = joinRequest.RequestStatus,
                    TripId = joinRequest.TripId,
                };



                return new Response<JoinRequestDto>() { isSuccess = false, Data = returnDto, List = null, Message = "Success", Status = 200 };

            }
            catch (Exception ex)
            {

                return new Response<JoinRequestDto>() { isSuccess = false, Data = null, List = null, Message = ex.Message, Status = 500 };
            }




        }

    
    }
}
