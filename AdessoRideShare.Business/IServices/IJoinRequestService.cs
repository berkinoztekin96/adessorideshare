﻿using AdessoRideShare.Common.Dto;
using AdessoRideShare.Common.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdessoRideShare.Business.IServices
{
    public interface IJoinRequestService
    {
        Task<Response<JoinRequestDto>> CreateJoinRequest(int tripId);
        Task<Response<JoinRequestDto>> GetAllJoinRequests(int tripId);

        Task<Response<JoinRequestDto>> ConsiderRequest(int requestId, int status);
    }
}
