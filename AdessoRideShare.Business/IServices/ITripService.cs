﻿using AdessoRideShare.Common.Dto;
using AdessoRideShare.Common.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdessoRideShare.Business.IServices
{
    public interface ITripService
    {

        Task<Response<TripDto>> GetAllTrips();
        Task<Response<TripDto>> GetTripById(int id);
        Task<Response<TripDto>> CreateTrip(CreateTripDto dto);
        Task<Response<TripDto>> UpdateTrip(UpdateTripDto dto);
        Task<Response<TripDto>> DeleteTrip(int id);
        Task<Response<TripDto>> SearchTrip(TripSearchDto dto);
    }
}
