﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Common.Enum
{
    public enum RequestStatusEnum
    {
        Waiting = 1,
        Accepted = 2,
        Rejected = 3
      
    }
}
