﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Common.Dto
{
    public class TripDto
    {
        public int Id { get; set; }
        public DateTime TripDate { get; set; }
        public string FromCity { get; set; }
        public string WhereCity { get; set; }
        public int TotalSeatNumber { get; set; }
        public int RemainingSeats { get; set; }
        public string Description { get; set; }
        public string Route { get; set; }
    }
}
