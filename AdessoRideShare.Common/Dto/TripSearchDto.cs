﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Common.Dto
{
    public class TripSearchDto
    {
        public DateTime TripDate { get; set; }
        public string FromCity { get; set; }
        public string WhereCity { get; set; }

    }
}
