﻿using AdessoRideShare.Common.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdessoRideShare.Common.Dto
{
   public  class JoinRequestDto
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public RequestStatusEnum RequestStatus { get; set; }
        public int TripId { get; set; }
    }
}
