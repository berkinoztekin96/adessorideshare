# AdessoRideShare

- .Net Core 3.1 version and relevant libraries are used in this project
- AdessoRideShare project has 5 main layers
- Postman collection v2.1 is available as AdessoRideShare.postman_collection.json. To access all end points kindly import the json file
- All migration files is inside of Repository Layer

- All API's have a common response type called Response

# Response
    {"Status":200,
    "isSuccess": true,
    "message": "",
    "data": {},
    "dataList":[]
    }

 # Relations
- JoinRequest and Trip entities were created with one-to-zero relationship. **Which means primary key of Trip is foreign key of JoinRequest**
- Trip entity can be deleted with soft delete (Makes their IsActive false)
- Windows Sql Server is used as database

# Creating Trip
When users are going to create a trip with their own car, they have to enter the departure routes as well as the start and end dates. (For the optional part in the 2nd section)

# Searching Trip
Passengers who want to join the journey write their start and end cities, and the logic of the algorithm to work first lists the journeys between those dates in their memory. Since the roads to which the vehicle will go are already known, there is a list of cities that are adjacent to these cities. Displays this list from the City table in the database. If there is one of these neighboring lists in the vehicle's route and the ending route of both the vehicle and the passenger is the same, this journey is now visible to the passenger. I assumed that 200 cities and their and adjacents are in my City table. (While developing I just created enough sample data.)

# Before Run

- Change your connection string in appsettings.json
- Run "Update-Database" command in Package Manager Console
